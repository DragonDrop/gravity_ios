//
//  AppDelegate.h
//  Gravity
//
//  Created by Kevin Phillips on 12/9/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
