//
//  Body.h
//  Gravity
//
//  Created by Kevin Phillips on 12/10/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Body : NSObject
{
    @public
    float _vx;
    float _vy;
    float _px;
    float _py;
    float _fx;
    float _fy;
    float _mass;
    
    float _r;
    float _g;
    float _b;
}
-(void) Update:(float) dt;
-(void) AddForce:(Body*) body;
-(float) Distance:(Body*) body;
-(Body*) Merge: (Body*) body;

@end
