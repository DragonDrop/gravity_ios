//
//  BHTreeGLES.h
//  Gravity
//
//  Created by Kevin Phillips on 12/25/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import "BHTree.h"

@interface BHTreeGLES : BHTree
    -(void) Draw;
@end
