//
//  BHTree.m
//  Gravity
//
//  Created by Kevin Phillips on 12/10/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import "BHTree.h"

@implementation BHTree

    -(BHTree*) init:(BaseQuad*) quad
    {
        _quad = quad;
        return self;
    }

-(void) insert:(Body*) body
    {
        if(_body == NULL)
        {
            _body = body;
        }
        else if(_nw != NULL)
        {
            _body = [_body Merge:body];
            if([_nw->_quad Contains:body])
            {
                [_nw insert:body];
            }
            else if([_ne->_quad Contains:body])
            {
                [_ne insert:body];
            }
            else if([_sw->_quad Contains:body])
            {
                [_sw insert:body];
            }
            else if([_se->_quad Contains:body])
            {
                [_se insert:body];
            }
        }
        else
        {
            float size = _quad->_size / 2;
            
            NWQuad* nwQuad = [[NWQuad alloc] init:_quad->_left :_quad->_top :size];
            _nw = [[BHTree alloc] init: nwQuad ];
            
            NEQuad* neQuad = [[NEQuad alloc] init:_quad->_left+size :_quad->_top :size];
            _ne = [[BHTree alloc] init: neQuad ];
            
            SWQuad* swQuad = [[SWQuad alloc] init:_quad->_left :_quad->_top+size :size];
            _sw = [[BHTree alloc] init: swQuad ];
            
            SEQuad* seQuad = [[SEQuad alloc] init:_quad->_left+size :_quad->_top+size :size];
            _se = [[BHTree alloc] init: seQuad ];
            
            Body* temp = _body;
            [self insert:body];
            [self insert:temp];
            _body = [temp Merge:body];
        }
    }
    
    -(void) update:(Body*) body
    {
        if(_nw == NULL)
        {
            if(body == _body) return;
            [body AddForce:_body];
        }
        else if( _quad->_size / [_body Distance:body] < 0.5)
        {
            [body AddForce:_body];
        }
        else
        {
            if(_nw->_body != NULL) [_nw update:body];
            if(_ne->_body != NULL) [_ne update:body];
            if(_sw->_body != NULL) [_sw update:body];
            if(_se->_body != NULL) [_se update:body];
        }
    }

@end