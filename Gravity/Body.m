//
//  Body.m
//  Gravity
//
//  Created by Kevin Phillips on 12/10/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import "Body.h"

double G = 6.67e-11;

@implementation Body

-(void) Update: (float) dt
{
    _vx += dt * _fx / _mass;
    _vy += dt * _fy / _mass;
    _px += dt * _vx;
    _py += dt * _vy;
    _fx = 0;
    _fy = 0;
}

-(void) AddForce:(Body*) body
{
    double EPS = 3e4;
    double dx = body->_px - _px;
    double dy = body->_py - _py;
    double dist = sqrt(dx*dx+dy*dy);
    if(dist < 0.00001 && dist > -0.00001)
        dist = 1;
    double F = (G * _mass * body->_mass) / (dist * dist + EPS * EPS);
    _fx += F * dx / dist;
    _fy += F * dy / dist;
}

-(Body*) Merge: (Body*) body
{
    Body *mergedBody = [[Body alloc] init];
    mergedBody->_mass = _mass + body->_mass;
    mergedBody->_px = (_px * _mass + body->_px * body->_mass) / mergedBody->_mass;
    mergedBody->_py = (_py * _mass + body->_py * body->_mass) / mergedBody->_mass;
    mergedBody->_vx = _vx + body->_vx;
    mergedBody->_vy = _vy + body->_vy;
    return mergedBody;
}

-(float) Distance:(Body*) body
{
    double dx = _px - body->_px;
    double dy = _py - body->_py;
    return sqrt(dx*dx+dy*dy);
}

@end
