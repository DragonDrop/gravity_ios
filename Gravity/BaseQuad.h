//
//  BaseQuad.h
//  Gravity
//
//  Created by Kevin Phillips on 12/22/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Body.h"

@interface BaseQuad : NSObject
    {
        @public
        float _top;
        float _bottom;
        float _left;
        float _right;
        float _size;
    }
    -(BaseQuad *) init: (float)x :(float)y :(float)size;
    -(int) Contains:(Body *) body;
@end
