//
//  NEQuad.m
//  Gravity
//
//  Created by Kevin Phillips on 12/22/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import "NEQuad.h"

@implementation NEQuad

    -(int) Contains: (Body *) body
    {
        float x = body->_px;
        float y = body->_py;
        
		if ( x >= _left && x <= _right )
        {
            if( y > _top && y <= _bottom )
            {
                return 1;
            }
        }
		return 0;
    }
@end
