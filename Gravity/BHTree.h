//
//  BHTree.h
//  Gravity
//
//  Created by Kevin Phillips on 12/10/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Body.h"
#import "BaseQuad.h"
#import "NWQuad.h"
#import "NEQuad.h"
#import "SWQuad.h"
#import "SEQuad.h"

@interface BHTree: NSObject
{
    BHTree *_nw;
    BHTree *_ne;
    BHTree *_sw;
    BHTree *_se;
    BaseQuad *_quad;
    Body *_body;
}

    -(BHTree*) init:(BaseQuad*) quad;
    -(void) insert:(Body*) body;
    -(void) update:(Body*) body;
@end