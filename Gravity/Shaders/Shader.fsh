//
//  Shader.fsh
//  Gravity
//
//  Created by Kevin Phillips on 12/9/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

varying lowp vec4 DestinationColor;

void main(void)
{
    gl_FragColor = DestinationColor;
}