//
//  Shader.vsh
//  Gravity
//
//  Created by Kevin Phillips on 12/9/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

attribute vec4 position;
attribute vec4 color;

varying vec4 DestinationColor;

void main(void)
{
    DestinationColor = color;
    gl_Position = position;
}