//
//  ViewController.h
//  Gravity
//
//  Created by Kevin Phillips on 12/9/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>

@interface ViewController : GLKViewController

@end
