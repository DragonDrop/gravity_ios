//
//  BaseQuad.m
//  Gravity
//
//  Created by Kevin Phillips on 12/22/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import "BaseQuad.h"

@implementation BaseQuad
    -(BaseQuad*) init: (float)x :(float)y :(float)size;
    {
        _top = y;
        _bottom = y + size;
        _left = x;
        _right = x + size;
        _size = size;
        return self;
    }
    
    -(int) Contains: (Body *) body
    {
        float x = body->_px;
        float y = body->_py;
        
        if ( x >= _left && x <= _right )
        {
            if( y >= _top && y <= _bottom )
            {
                return 1;
            }
        }
        return 0;
    }
    
@end
