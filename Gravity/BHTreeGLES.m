//
//  BHTreeGLES.m
//  Gravity
//
//  Created by Kevin Phillips on 12/25/13.
//  Copyright (c) 2013 Kevin Phillips. All rights reserved.
//

#import "BHTreeGLES.h"

@implementation BHTreeGLES

    -(void)Draw
    {
        NSMutableArray *array = [[NSMutableArray alloc] init];
        [self FindNodes:self :array];
        
        int number = [array count];
        
        float treeNodes[number];
        float treeNodesColor[number];
        
        for(int i = 0; i < number; i++)
        {
            treeNodes[i] = [[array objectAtIndex:i] floatValue];
            treeNodesColor[i] = 1.0;
        }
        
        glEnableVertexAttribArray(GLKVertexAttribPosition);
        glEnableVertexAttribArray(GLKVertexAttribColor);
        
        glVertexAttribPointer(GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, 0, treeNodes);
        glVertexAttribPointer(GLKVertexAttribColor, 3, GL_FLOAT, GL_FALSE, 0, treeNodesColor);
        
        glDrawArrays(GL_LINES, 0, number/3);
        
        glDisableVertexAttribArray(GLKVertexAttribPosition);
        glDisableVertexAttribArray(GLKVertexAttribColor);
    }

    -(void)FindNodes:(BHTree*)tree :(NSMutableArray*)array
    {
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_left]];
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_top]];
        [array addObject:[NSNumber numberWithFloat:0]];
        
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_right]];
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_top]];
        [array addObject:[NSNumber numberWithFloat:0]];
        
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_left]];
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_bottom]];
        [array addObject:[NSNumber numberWithFloat:0]];
        
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_right]];
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_bottom]];
        [array addObject:[NSNumber numberWithFloat:0]];
        
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_left]];
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_top]];
        [array addObject:[NSNumber numberWithFloat:0]];
        
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_left]];
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_bottom]];
        [array addObject:[NSNumber numberWithFloat:0]];
        
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_right]];
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_top]];
        [array addObject:[NSNumber numberWithFloat:0]];
        
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_right]];
        [array addObject:[NSNumber numberWithFloat:tree->_quad->_bottom]];
        [array addObject:[NSNumber numberWithFloat:0]];
        
        if(tree->_nw != NULL)
            [self FindNodes:tree->_nw :array];
        if(tree->_ne != NULL)
            [self FindNodes:tree->_ne :array];
        if(tree->_sw != NULL)
            [self FindNodes:tree->_sw :array];
        if(tree->_se != NULL)
            [self FindNodes:tree->_se :array];
    }

@end
